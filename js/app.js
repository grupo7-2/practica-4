// Declaración de array con 20 valores enteros
const arrNum1=[15,20,8,7,4,3,12,21,9,18,6,1,10,18,22,26,13,28,24,11];

// Función 1 -Promedio-
/*1. Diseña una función que reciba como argumento un arreglo de valores 
enteros de 20 posiciones , regrese el valor promedio de los elementos del arreglo.*/
function promedioArr(arreglo1){
    let sumaArr = 0;

    for(let i = 0; i<arreglo1.length; i++){
        sumaArr = sumaArr + arreglo1[i];
    }
    return sumaArr/arreglo1.length;
}

// Declaración de array con 20 valores enteros
let arrNum=[15,20,8,7,4,3,12,21,9,18,6,1,10,18,22,26,13,28,24,11];

console.log("Función 1 - Promedio: "+promedioArr(arrNum));

// Función 2 -Pares-
/*2. Diseñe una función que reciba como argumento un arreglo de 20 valores numéricos enteros, y me regrese la cantidad de valores pares que existe en el arreglo*/

function paresArr(arreglo2){
    let sumaPares = 0;

    for(let i = 0; i<arreglo2.length; i++){
        if(arreglo2[i] % 2 === 0){
            sumaPares++;
        }
    }
    return sumaPares;
}

function imparesArr(arreglo2){
    let sumaPares = 0;

    for(let i = 0; i<arreglo2.length; i++){
        if(arreglo2[i] % 2 != 0){
            sumaPares++;
        }
    }
    return sumaPares;
}

console.log("Función 2 - Pares: "+paresArr(arrNum));

// Función 3 -Mayor a menor-
/*3. Diseñe una función que reciba como argumento un arreglo de 20 valores numéricos enteros, ordene los valores del arreglo de mayor a menor.*/

function mayMenArr(arreglo3){
    let bandera = false;

    while(!bandera){
        bandera = true;
        for(let i=0; i<arreglo3.length; i++){
            if(arreglo3[i] < arreglo3[i+1]){
                aux = arreglo3[i+1];
                arreglo3[i+1] = arreglo3[i];
                arreglo3[i] = aux;
                bandera = false;
            }
        }
    }
    return arreglo3;
}

// Función para ordenar ascendentemente
function arrASC(arreglo3){
    let bandera = false;

    while(!bandera){
        bandera = true;
        for(let i=0; i<arreglo3.length; i++){
            if(arreglo3[i] > arreglo3[i+1]){
                aux = arreglo3[i+1];
                arreglo3[i+1] = arreglo3[i];
                arreglo3[i] = aux;
                bandera = false;
            }
        }
    }
    return arreglo3;
}

console.log("Función 3 - Mayor a menor: " + mayMenArr(arrNum));

// Función para mostrar los resultados de las funciones 1, 2 y 3
function mostrarRes(){
    arr1=document.getElementById('arreglo1');
    fun1=document.getElementById('Funcion1');
    fun2=document.getElementById('Funcion2');
    fun3=document.getElementById('Funcion3');
    let promedio=promedioArr(arrNum);
    let pares=paresArr(arrNum);
    let mayorYmenor=mayMenArr(arrNum);
    arr1.innerHTML=arrNum1;
    fun1.innerHTML=promedio;
    fun2.innerHTML=pares;
    fun3.innerHTML=mayorYmenor;
}

function llenar(){
    var limite = document.getElementById('limite').value;
    var Listanumeros = document.getElementById('numeros');
    var arreglo3 = new Array();
    porcentajePar = document.getElementById('porPares');
    porcentajeImpar = document.getElementById('porImpares');
    simetriaRes = document.getElementById('EsSimetrico');

    while (Listanumeros.options.length > 0) {
        Listanumeros.remove(0);
    }

    for(let con=0; con<limite; con++){
        let aleatorio = Math.floor(Math.random()*(50-1+1)+1);
        arreglo3[con] = aleatorio;
        arrASC(arreglo3);
    }

    for(let con=0; con<limite; con++){
        Listanumeros.options[con] = new Option(arreglo3[con],'valor:'+ con);
    }

    let numPares = paresArr(arreglo3);
    let numImpares = imparesArr(arreglo3);
    let total = numPares+numImpares;
    let porcPares = (numPares/total)*100;
    let porcImpares = (numImpares/total)*100;

    porcentajePar.innerHTML = porcPares.toFixed(2) + "%";
    porcentajeImpar.innerHTML = porcImpares.toFixed(2) + "%";

    // Condición para determinar la simetría
    if(porcPares - porcImpares > 25 || porcImpares - porcPares > 25){
        simetriaRes.innerHTML = "No es simétrico";
    }else{
        simetriaRes.innerHTML = "Es simétrico";
    }

}

function validar(){
    validarLimite = document.querySelector('#limite').value;
    errorLimite = document.querySelector("#errorLimite");

    // Condición ? si no
    validarLimite == "" ? errorLimite.style.visibility = 'visible' 
    : errorLimite.style.visibility = 'hidden';

    if(validarLimite==0){
        alert('Se necesita llenar el campo de texto con un número válido.');
        porcentajePar.innerHTML = "";
        porcentajeImpar.innerHTML = "";
        simetriaRes.innerHTML = "";
    }

}